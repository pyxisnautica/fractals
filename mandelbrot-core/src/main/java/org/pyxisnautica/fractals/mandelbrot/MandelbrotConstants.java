package org.pyxisnautica.fractals.mandelbrot;

public class MandelbrotConstants {

	public static final double RE_MIN = -2.5;
	
	public static final double RE_MAX = 1;
	
	public static final double IM_MIN = -1;
	
	public static final double IM_MAX = 1;
	
}
