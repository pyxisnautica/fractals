package org.pyxisnautica.fractals.mandelbrot;

import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.mandelbrot.impl.MandelbrotService;

public class MandelbrotServiceFactory {

	public static FractalService getInstance() {
		return INSTANCE;
	}
	
	private MandelbrotServiceFactory() {}
	
	private static final FractalService INSTANCE = new MandelbrotService();
	
}
