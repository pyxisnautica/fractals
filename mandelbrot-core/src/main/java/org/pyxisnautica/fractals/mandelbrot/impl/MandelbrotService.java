package org.pyxisnautica.fractals.mandelbrot.impl;

import java.util.HashSet;
import java.util.Set;

import org.pyxisnautica.fractals.api.Constants;
import org.pyxisnautica.fractals.api.Fractal;
import org.pyxisnautica.fractals.api.FractalBounds;
import org.pyxisnautica.fractals.api.FractalMeta;
import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.api.Point;
import org.pyxisnautica.fractals.api.ViewportDimensions;
import org.pyxisnautica.fractals.mandelbrot.MandelbrotConstants;

public class MandelbrotService implements FractalService {
		
	private static final double ESCAPE_RADIUS_SQUARED = 4;
	
	private static final String ABOUT = "Mandelbrot set plotter made by pyxisnautica.org.";
	
	@Override
	public String about() {
		return String.format(ABOUT, MandelbrotConstants.RE_MIN, MandelbrotConstants.IM_MIN, MandelbrotConstants.RE_MAX, MandelbrotConstants.IM_MAX, Constants.VP_MAX_X, Constants.VP_MAX_Y, Constants.RE_DIM_MIN, Constants.IM_DIM_MIN, Constants.VP_MIN_X, Constants.VP_MIN_Y);
	}
	
	@Override
	public Fractal getFractal(FractalMeta fractalMeta) {
		FractalBounds fractalBounds = fractalMeta.getFractalBounds();
		ViewportDimensions viewportBounds = fractalMeta.getViewportBounds();
		
		check(fractalBounds, viewportBounds);
		Set<Point> points = new HashSet<>();
		
		int xMin = 0;
		int xMax = viewportBounds.getX1();
		int yMin = 0;
		int yMax = viewportBounds.getY1();
		
		double reMin = Math.min(fractalBounds.getRe1(), fractalBounds.getRe2());
		double reMax = Math.max(fractalBounds.getRe1(), fractalBounds.getRe2());
		double imMin = Math.min(fractalBounds.getIm1(), fractalBounds.getIm2());
		double imMax = Math.max(fractalBounds.getIm1(), fractalBounds.getIm2());
		
		double reStep = (reMax - reMin) / (xMax - xMin);
		double imStep = (imMax - imMin) / (yMax - yMin);
		
		int curX = xMin;
		int curY = yMin;
		
		for (double re = reMin; re < reMax; re += reStep) {
			for (double im = imMin; im < imMax; im += imStep) {
				
				int curIter = 0;
				double curRe = 0;
				double curIm = 0;
				
				double curReSq;
				double curImSq;
				
				while ((curReSq = Math.pow(curRe, 2)) + (curImSq = Math.pow(curIm, 2)) < ESCAPE_RADIUS_SQUARED && curIter < Constants.ITER_MAX) {
					double tmpRe = curReSq - curImSq + re;
					curIm = 2*curRe*curIm + im;
					curRe = tmpRe;
					curIter++;
				}
				
				points.add(new Point(curX, curY, curIter));
				curY++;
			}
			curX++;
			curY = 0;
		}
		
		return new Fractal(points, fractalMeta);
	}

	private void check(FractalBounds bounds, ViewportDimensions viewport) {
		boolean validFractalBounds = false;
		if ((bounds.getRe1() >= MandelbrotConstants.RE_MIN && bounds.getRe2() >= MandelbrotConstants.RE_MIN && bounds.getRe1() <= MandelbrotConstants.RE_MAX && bounds.getRe2() <= MandelbrotConstants.RE_MAX && Math.abs(bounds.getRe2() - bounds.getRe1()) >= Constants.RE_DIM_MIN)) {
			if ((bounds.getIm1() >= MandelbrotConstants.IM_MIN && bounds.getIm2() >= MandelbrotConstants.IM_MIN && bounds.getIm1() <= MandelbrotConstants.IM_MAX && bounds.getIm2() <= MandelbrotConstants.IM_MAX && Math.abs(bounds.getIm2() - bounds.getIm1()) >= Constants.IM_DIM_MIN)) {
				validFractalBounds = true;
			}
		}
		
		if (!validFractalBounds) {
			throw new IllegalArgumentException(String.format(
					"Fractal bounds must be between [%f, %f] on the real axis and [%f, %f] on the imaginary with minimum real size %f and minimum imaginary size %f",
					MandelbrotConstants.RE_MIN, MandelbrotConstants.RE_MAX, MandelbrotConstants.IM_MIN, MandelbrotConstants.IM_MAX, Constants.RE_DIM_MIN, Constants.IM_DIM_MIN));
		}
		
		boolean validViewportBounds = false;
		if (viewport.getX1() <= Constants.VP_MAX_X && viewport.getX1() >= Constants.VP_MIN_X) {
			if (viewport.getY1() <= Constants.VP_MAX_Y && viewport.getY1() >= Constants.VP_MIN_Y) {
				validViewportBounds = true;
			}	
		}
		
		if (!validViewportBounds) {
			throw new IllegalArgumentException(String.format(
					"Viewport bounds must be between [%d, %d] on the X axis and [%d, %d] on the Y",
					Constants.VP_MIN_X, Constants.VP_MAX_X, Constants.VP_MIN_Y, Constants.VP_MAX_Y));
		}
	}	
}
