package org.pyxisnautica.fractals.mandelbrot.impl;

import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.image.BaseFractalImageService;
import org.pyxisnautica.fractals.mandelbrot.MandelbrotServiceFactory;

public class MandelbrotImageService extends BaseFractalImageService {

	public MandelbrotImageService(FractalService fractalService) {
		super(MandelbrotServiceFactory.getInstance());
	}

}
