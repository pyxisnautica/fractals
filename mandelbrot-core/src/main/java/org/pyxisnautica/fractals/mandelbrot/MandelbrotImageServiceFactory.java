package org.pyxisnautica.fractals.mandelbrot;

import org.pyxisnautica.fractals.api.FractalImageService;
import org.pyxisnautica.fractals.mandelbrot.impl.MandelbrotImageService;

public class MandelbrotImageServiceFactory {

	private static final FractalImageService INSTANCE = new MandelbrotImageService(MandelbrotServiceFactory.getInstance());
	
	public static FractalImageService getInstance() {
		return INSTANCE;
	}
	
}
