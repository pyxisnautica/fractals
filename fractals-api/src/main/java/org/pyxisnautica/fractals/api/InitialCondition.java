package org.pyxisnautica.fractals.api;

public class InitialCondition {

	private final double re;
	
	private final double im;
	
	public InitialCondition(double re, double im) {
		super();
		this.re = re;
		this.im = im;
	}

	public double getRe() {
		return re;
	}

	public double getIm() {
		return im;
	}
	
}
