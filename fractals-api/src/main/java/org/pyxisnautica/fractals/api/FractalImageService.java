package org.pyxisnautica.fractals.api;

public interface FractalImageService {

	byte[] toImage(Fractal fractal);
	
}
