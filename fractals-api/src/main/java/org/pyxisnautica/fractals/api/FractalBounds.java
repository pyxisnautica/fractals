package org.pyxisnautica.fractals.api;

public class FractalBounds {

	private final double re1;
	
	private final double im1;
	
	private final double re2;
	
	private final double im2;

	public FractalBounds(double re1, double im1, double re2, double im2) {
		super();
		this.re1 = re1;
		this.im1 = im1;
		this.re2 = re2;
		this.im2 = im2;
	}

	public double getRe1() {
		return re1;
	}

	public double getIm1() {
		return im1;
	}

	public double getRe2() {
		return re2;
	}

	public double getIm2() {
		return im2;
	}

}
