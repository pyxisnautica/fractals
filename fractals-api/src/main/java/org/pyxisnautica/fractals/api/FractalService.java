package org.pyxisnautica.fractals.api;

public interface FractalService {

	public Fractal getFractal(FractalMeta fractalMeta);

	public String about();
}
