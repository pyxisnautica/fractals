package org.pyxisnautica.fractals.api;

public class Point {

	private final int x;
	
	private final int y;
	
	private final int iter;

	public Point(int x, int y, int iter) {
		super();
		this.x = x;
		this.y = y;
		this.iter = iter;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getIter() {
		return iter;
	}
	
}
