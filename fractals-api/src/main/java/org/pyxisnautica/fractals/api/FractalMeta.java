package org.pyxisnautica.fractals.api;

public class FractalMeta {

	private final FractalBounds fractalBounds;
	
	private final ViewportDimensions viewportBounds;
	
	private final InitialCondition initialCondition;

	public FractalMeta(FractalBounds fractalBounds, ViewportDimensions viewportBounds, InitialCondition initialCondition) {
		super();
		this.fractalBounds = fractalBounds;
		this.viewportBounds = viewportBounds;
		this.initialCondition = initialCondition;
	}

	public FractalBounds getFractalBounds() {
		return fractalBounds;
	}

	public ViewportDimensions getViewportBounds() {
		return viewportBounds;
	}

	public InitialCondition getInitialCondition() {
		return initialCondition;
	}
	
}
