package org.pyxisnautica.fractals.api;

public class ViewportDimensions {

	private final int width;
	
	private final int height;
	
	public ViewportDimensions(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}

	public int getX1() {
		return width;
	}

	public int getY1() {
		return height;
	}

	@Override
	public String toString() {
		return String.format("[%d, %d]", width, height);
	}
}
