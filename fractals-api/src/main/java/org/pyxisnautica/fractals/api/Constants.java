package org.pyxisnautica.fractals.api;

public class Constants {

	public static final int ITER_MAX = 120;

	public static final int VP_MAX_X = 1250;

	public static final int VP_MAX_Y = 1250;
	
	public static final int VP_MIN_X = 64;
	
	public static final int VP_MIN_Y = 64;

	public static final int VP_DEFAULT_X = 640;
	
	public static final int VP_DEFAULT_Y = 480;
	
	public static final double RE_DIM_MIN = 0.2;
	
	public static final double RE_DIM_MAX = 0.2;
	
	public static final double IM_DIM_MIN = 0.2;
	
	public static final double IM_DIM_MAX = 0.2;
	
}
