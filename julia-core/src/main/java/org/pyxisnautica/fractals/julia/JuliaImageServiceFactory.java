package org.pyxisnautica.fractals.julia;

import org.pyxisnautica.fractals.api.FractalImageService;
import org.pyxisnautica.fractals.julia.impl.JuliaImageService;

public class JuliaImageServiceFactory {

	private static final FractalImageService INSTANCE = new JuliaImageService(JuliaServiceFactory.getInstance());
	
	public static FractalImageService getInstance() {
		return INSTANCE;
	}
	
}
