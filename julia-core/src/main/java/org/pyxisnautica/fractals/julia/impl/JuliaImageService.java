package org.pyxisnautica.fractals.julia.impl;

import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.image.BaseFractalImageService;
import org.pyxisnautica.fractals.julia.JuliaServiceFactory;

public class JuliaImageService extends BaseFractalImageService {

	public JuliaImageService(FractalService fractalService) {
		super(JuliaServiceFactory.getInstance());
	}

	

}
