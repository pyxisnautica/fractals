package org.pyxisnautica.fractals.julia.impl;

import java.util.HashSet;
import java.util.Set;

import org.pyxisnautica.fractals.api.Constants;
import org.pyxisnautica.fractals.api.Fractal;
import org.pyxisnautica.fractals.api.FractalBounds;
import org.pyxisnautica.fractals.api.FractalMeta;
import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.api.InitialCondition;
import org.pyxisnautica.fractals.api.Point;
import org.pyxisnautica.fractals.api.ViewportDimensions;
import org.pyxisnautica.fractals.julia.JuliaConstants;

public class JuliaService implements FractalService {

	private static final double ESCAPE_RADIUS_SQUARED = 4;
	
	private static final String ABOUT = "Julia set plotter made by pyxisnautica.org.";
	
	@Override
	public String about() {
		return ABOUT;
	}
	
	@Override
	public Fractal getFractal(FractalMeta fractalMeta) {
		FractalBounds fractalBounds = fractalMeta.getFractalBounds();
		ViewportDimensions viewportBounds = fractalMeta.getViewportBounds();
		InitialCondition c = fractalMeta.getInitialCondition();
		
		check(fractalBounds, viewportBounds, c);
		Set<Point> points = new HashSet<>();
		
		int xMin = 0;
		int xMax = viewportBounds.getX1();
		int yMin = 0;
		int yMax = viewportBounds.getY1();
		
		double reMin = Math.min(fractalBounds.getRe1(), fractalBounds.getRe2());
		double reMax = Math.max(fractalBounds.getRe1(), fractalBounds.getRe2());
		double imMin = Math.min(fractalBounds.getIm1(), fractalBounds.getIm2());
		double imMax = Math.max(fractalBounds.getIm1(), fractalBounds.getIm2());
		
		double reStep = (reMax - reMin) / (xMax - xMin);
		double imStep = (imMax - imMin) / (yMax - yMin);
		
		int curX = xMin;
		int curY = yMin;
		
		for (double re = reMin; re < reMax; re += reStep) {
			for (double im = imMin; im < imMax; im += imStep) {
				
				int curIter = 0;
				double curRe = re;
				double curIm = im;
				
				double curReSq;
				double curImSq;
				
				while ((curReSq = Math.pow(curRe, 2)) + (curImSq = Math.pow(curIm, 2)) < ESCAPE_RADIUS_SQUARED && curIter < Constants.ITER_MAX) {
					double tmpRe = curReSq - curImSq;
					curIm = 2*curRe*curIm + c.getIm();
					curRe = tmpRe + c.getRe();
					curIter++;
				}
				
				points.add(new Point(curX, curY, curIter));
				curY++;
			}
			curX++;
			curY = 0;
		}
		
		return new Fractal(points, fractalMeta);
	}

	private void check(FractalBounds bounds, ViewportDimensions viewport, InitialCondition c) {
		boolean validFractalBounds = false;
		if ((bounds.getRe1() >= JuliaConstants.RE_MIN && bounds.getRe2() >= JuliaConstants.RE_MIN && bounds.getRe1() <= JuliaConstants.RE_MAX && bounds.getRe2() <= JuliaConstants.RE_MAX && Math.abs(bounds.getRe2() - bounds.getRe1()) >= Constants.RE_DIM_MIN)) {
			if ((bounds.getIm1() >= JuliaConstants.IM_MIN && bounds.getIm2() >= JuliaConstants.IM_MIN && bounds.getIm1() <= JuliaConstants.IM_MAX && bounds.getIm2() <= JuliaConstants.IM_MAX && Math.abs(bounds.getIm2() - bounds.getIm1()) >= Constants.IM_DIM_MIN)) {
				validFractalBounds = true;
			}
		}
		
		if (!validFractalBounds) {
			throw new IllegalArgumentException(String.format(
					"Fractal bounds must be between [%f, %f] on the real axis and [%f, %f] on the imaginary with minimum real size %f and minimum imaginary size %f",
					JuliaConstants.RE_MIN, JuliaConstants.RE_MAX, JuliaConstants.IM_MIN, JuliaConstants.IM_MAX, Constants.RE_DIM_MIN, Constants.IM_DIM_MIN));
		}
		
		boolean validViewportBounds = false;
		if (viewport.getX1() >= Constants.VP_MIN_X && viewport.getX1() >= Constants.VP_MIN_X && viewport.getX1() <= Constants.VP_MAX_X) {
			if (viewport.getY1() >= Constants.VP_MIN_Y && viewport.getY1() >= Constants.VP_MIN_X && viewport.getX1() <= Constants.VP_MAX_Y) {
				validViewportBounds = true;
			}	
		}
		
		if (!validViewportBounds) {
			throw new IllegalArgumentException(String.format(
					"Viewport bounds must be between [%d, %d] on the X axis and [%d, %d] on the Y",
					Constants.VP_MIN_X, Constants.VP_MAX_X, Constants.VP_MIN_Y, Constants.VP_MAX_Y));
		}
		
		boolean validInitialCondition = false;
		if (Math.pow(c.getRe(), 2) + Math.pow(c.getIm(), 2) <= JuliaConstants.INITIAL_CONDITION_MAX_MODULUS * JuliaConstants.INITIAL_CONDITION_MAX_MODULUS) {
			validInitialCondition = true;
		}
		
		if (!validInitialCondition) {
			throw new IllegalArgumentException(String.format("Initial condition must have a modulus <= 2", JuliaConstants.INITIAL_CONDITION_MAX_MODULUS));
		}
	}
}
