package org.pyxisnautica.fractals.julia;

public class JuliaConstants {

	public static final double C_RE_DEFAULT = -0.4;
	
	public static final double C_IM_DEFAULT = 0.6;

	public static final double RE_MIN = -2.5;
	
	public static final double RE_MAX = 2.5;
	
	public static final double IM_MIN = -1;
	
	public static final double IM_MAX = 1;

	public static final double INITIAL_CONDITION_MAX_MODULUS = 2;
	
}
