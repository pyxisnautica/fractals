package org.pyxisnautica.fractals.julia;

import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.julia.impl.JuliaService;

public class JuliaServiceFactory {

	public static FractalService getInstance() {
		return INSTANCE;
	}
	
	private JuliaServiceFactory() {}
	
	private static final FractalService INSTANCE = new JuliaService();
	
}
