package org.pyxisnautica.fractals.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.pyxisnautica.fractals.api.Fractal;
import org.pyxisnautica.fractals.api.FractalImageService;
import org.pyxisnautica.fractals.api.FractalMeta;
import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.api.Point;
import org.pyxisnautica.fractals.api.ViewportDimensions;

public abstract class BaseFractalImageService implements FractalImageService {

	private static final String FORMAT = "png";

	private static final int RGB_MAX_VAL = 255;
	
	private static final int RGB_MIN_VAL = 0;
	
	private FractalService fractalService;
	
	public BaseFractalImageService(FractalService fractalService) {
		this.fractalService = fractalService;
	}
	
	public byte[] toImage(FractalMeta fractalMeta) {
		return toImage(fractalService.getFractal(fractalMeta));
	}
	
	private Map<Integer, Integer> toHistogram(Set<Point> points) {
		Map<Integer, Integer> histogram = new HashMap<>();
		for (Point p : points) {
			int iter = p.getIter();
			histogram.put(iter, histogram.getOrDefault(iter, 0) + 1);
		}
		return histogram;
	}
	
	private int maxIter(Map<Integer, Integer> histogram) {
		int max = 0;
		int iter = 0;
		for (Map.Entry<Integer, Integer> e : histogram.entrySet()) {
			if (e.getValue() > max) {
				max = 0;
				iter = e.getKey();
			}
		}
		return iter;
	}
	
	@Override
	public byte[] toImage(Fractal fractal) {
		byte[] image;
		Map<Integer, Integer> histogram = toHistogram(fractal.getPoints());
		ViewportDimensions bounds = fractal.getViewportBounds();
		try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			int width = bounds.getX1();
			int height = bounds.getY1();
			int xMin = 0;
			int yMin = 0;
			
			double maxFreq = Math.log(maxIter(histogram));
			
			Map<Integer, Double> logLookup = new HashMap<>();
			for (Map.Entry<Integer, Integer> e : histogram.entrySet()) {
				logLookup.put(e.getKey(), Math.log(e.getValue()));
			}
			
			double gamma = 1.5;
			
			Set<Point> adjusted = new HashSet<>();
			
			int maxIter = 0;
			
			for (Point p : fractal.getPoints()) {
				double alpha = logLookup.get(p.getIter()) / maxFreq;
				double corrected = p.getIter() * Math.pow(alpha, (1 / gamma));
				
				int newIter = (int) corrected;
				if (newIter > maxIter) {
					maxIter = newIter;
				}
				adjusted.add(new Point(p.getX(), p.getY(), newIter));
			}
			
			Map<Integer, Integer> colours = new HashMap<Integer, Integer>();
			for (int i = 0; i < maxIter; i++) {
				int val = RGB_MAX_VAL - ((int) ((((double) i / (double) maxIter)) * (RGB_MAX_VAL - RGB_MIN_VAL)) + RGB_MIN_VAL);
				colours.put(i, new Color(val, val, val).getRGB());
			}
			
			BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_USHORT_GRAY);
			
			for (Point p : adjusted) {
				int coordX = p.getX() - xMin;
				int coordY = p.getY() - yMin;
				if (coordX < width && coordY < height) {
					bi.setRGB(coordX, coordY, colours.getOrDefault(p.getIter(), 0));
				}
			}
			
			ImageIO.write(bi, FORMAT, os);
			os.flush();
			
			image = os.toByteArray();
			os.close();
			
			return image;
		} catch (Exception e) {
			throw new RuntimeException(String.format("Could not get byte array for image with viewport %s", bounds.toString()), e);
		}
	}
}
