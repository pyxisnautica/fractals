package org.pyxisnautica.fractals.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RestExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		return Response.status(Response.Status.BAD_REQUEST).entity(String.format("An error occurred. Please check your request and try again.")).type(MediaType.TEXT_PLAIN).build();
	}

}
