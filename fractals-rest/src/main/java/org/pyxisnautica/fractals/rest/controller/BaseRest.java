package org.pyxisnautica.fractals.rest.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

public abstract class BaseRest {

	protected Response toImageResponse(byte[] image) {
		return Response.status(Response.Status.OK)
				.entity(new StreamingOutput() {
					
					@Override
					public void write(OutputStream output) throws IOException, WebApplicationException {
						output.write(image);
						output.flush();
					}
				}).build();
	}
	
	protected Response about(String about) {
		return Response.status(Response.Status.OK).entity(about).build();
	}
	
}
