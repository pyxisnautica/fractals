package org.pyxisnautica.fractals.rest.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.pyxisnautica.fractals.api.Constants;
import org.pyxisnautica.fractals.api.Fractal;
import org.pyxisnautica.fractals.api.FractalBounds;
import org.pyxisnautica.fractals.api.FractalImageService;
import org.pyxisnautica.fractals.api.FractalMeta;
import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.api.InitialCondition;
import org.pyxisnautica.fractals.api.ViewportDimensions;
import org.pyxisnautica.fractals.julia.JuliaConstants;
import org.pyxisnautica.fractals.julia.JuliaImageServiceFactory;
import org.pyxisnautica.fractals.julia.JuliaServiceFactory;

@Path("/julia")
public class JuliaRest extends BaseRest {

	private final FractalImageService fractalImageService = JuliaImageServiceFactory.getInstance();
	
	private final FractalService fractalService = JuliaServiceFactory.getInstance();
		
	@GET
	@Path("image")
	@Produces("image/png")
	public Response getImage(
			@QueryParam("viewportWidth") Integer viewportWidth,
			@QueryParam("viewportHeight") Integer viewportHeight,
			@QueryParam("reMin") Double reMin,
			@QueryParam("reMax") Double reMax,
			@QueryParam("imMin") Double imMin,
			@QueryParam("imMax") Double imMax,
			@QueryParam("cRe") Double cRe,
			@QueryParam("cIm") Double cIm) {
		
		ViewportDimensions viewportBounds = new ViewportDimensions(
				viewportWidth == null ? Constants.VP_DEFAULT_X : viewportWidth, 
				viewportHeight == null ? Constants.VP_DEFAULT_Y : viewportHeight);
		
		FractalBounds bounds = new FractalBounds(
				reMin == null ? JuliaConstants.RE_MIN : reMin, 
				imMin == null ? JuliaConstants.IM_MIN : imMin, 
				reMax == null ? JuliaConstants.RE_MAX : reMax, 
				imMax == null ? JuliaConstants.IM_MAX : imMax);
		
		InitialCondition initialCondition = new InitialCondition(
				cRe == null ? JuliaConstants.C_RE_DEFAULT : cRe,
				cIm == null ? JuliaConstants.C_IM_DEFAULT : cIm);
		
		Fractal fractal = fractalService.getFractal(new FractalMeta(bounds, viewportBounds, initialCondition));
		
		return toImageResponse(fractalImageService.toImage(fractal));
	}
	
	@GET
	@Path("about")
	@Produces(MediaType.TEXT_PLAIN)
	public Response about() {
		return super.about(fractalService.about());
		
	}
}
