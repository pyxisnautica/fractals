package org.pyxisnautica.fractals.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RestIllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

	@Override
	public Response toResponse(IllegalArgumentException exception) {
		return Response.status(Response.Status.BAD_REQUEST).entity(String.format("An error occurred: %s. Please check your request and try again.", exception.getMessage())).type(MediaType.TEXT_PLAIN).build();
	}

}
