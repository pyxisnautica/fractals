package org.pyxisnautica.fractals.rest.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.pyxisnautica.fractals.api.Constants;
import org.pyxisnautica.fractals.api.Fractal;
import org.pyxisnautica.fractals.api.FractalBounds;
import org.pyxisnautica.fractals.api.FractalImageService;
import org.pyxisnautica.fractals.api.FractalMeta;
import org.pyxisnautica.fractals.api.FractalService;
import org.pyxisnautica.fractals.api.ViewportDimensions;
import org.pyxisnautica.fractals.mandelbrot.MandelbrotConstants;
import org.pyxisnautica.fractals.mandelbrot.MandelbrotImageServiceFactory;
import org.pyxisnautica.fractals.mandelbrot.MandelbrotServiceFactory;

@Path("/mandelbrot")
public class MandelbrotRest extends BaseRest {

	private final FractalImageService fractalImageService = MandelbrotImageServiceFactory.getInstance();
	
	private final FractalService fractalService = MandelbrotServiceFactory.getInstance();
		
	@GET
	@Path("image")
	@Produces("image/png")
	public Response getImage(
			@QueryParam("viewportWidth") Integer viewportWidth,
			@QueryParam("viewportHeight") Integer viewportHeight,
			@QueryParam("reMin") Double reMin,
			@QueryParam("reMax") Double reMax,
			@QueryParam("imMin") Double imMin,
			@QueryParam("imMax") Double imMax) {
		
		ViewportDimensions viewportBounds = new ViewportDimensions(
				viewportWidth == null ? Constants.VP_DEFAULT_X : viewportWidth, 
				viewportHeight == null ? Constants.VP_DEFAULT_Y : viewportHeight);
		
		FractalBounds bounds = new FractalBounds(
				reMin == null ? MandelbrotConstants.RE_MIN : reMin, 
				imMin == null ? MandelbrotConstants.IM_MIN : imMin, 
				reMax == null ? MandelbrotConstants.RE_MAX : reMax, 
				imMax == null ? MandelbrotConstants.IM_MAX : imMax);
		
		Fractal fractal = fractalService.getFractal(new FractalMeta(bounds, viewportBounds, null));
		
		return toImageResponse(fractalImageService.toImage(fractal));
	}
	
	@GET
	@Path("about")
	@Produces(MediaType.TEXT_PLAIN)
	public Response about() {
		return super.about(fractalService.about());
	}
}
